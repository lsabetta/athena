#include "TrigT1TGC/LVL1TGCTrigger.h"
#include "TrigT1TGC/MakeCoincidenceOut.h"

typedef LVL1TGCTrigger::LVL1TGCTrigger Lvl1LVL1TGCTrigger;
DECLARE_COMPONENT( Lvl1LVL1TGCTrigger )
typedef LVL1TGCTrigger::MakeCoincidenceOut Lvl1MakeCoincidenceOut;
DECLARE_COMPONENT( Lvl1MakeCoincidenceOut )

